#include <stdio.h>
#include <string.h>

int main() {
  FILE *file;
  char chr;

  file = fopen("test.txt", "r");

  // Test if error while reading file
  if (file == NULL) {
    puts("Can't open file.");
  }

  // Reading file char by char
  do {
    chr = fgetc(file);
    printf("%c", chr);
  } while (chr != EOF);

  // Closing the file
  fclose(file);
  return 0;
}
