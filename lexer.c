/**
 * @file lexer.c
 * @brief This program will lex the input file for beluga uses
 * @author Dorian Hamdani, Rémi Schiera, Arthur Barraux
 */

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
** Find out if an element is a delimiter
*/

bool is_delimiter(char element) {
  return (element == ' ' || element == '+' || element == '-' ||
          element == '*' || element == '/' || element == ',' ||
          element == ':' || element == '=');
}

bool id_operator(char *element) {
  return (strcmp(element, "+") == 0 || strcmp(element, "-") == 0 ||
          strcmp(element, "/") == 0 || strcmp(element, "*") == 0 ||
          strcmp(element, "<") == 0 || strcmp(element, ">") == 0 ||
          strcmp(element, "==") == 0);
}

bool is_valid_identifier(char *str) {
  return (str[0] != '0' && str[0] != '1' && str[0] != '2' && str[0] != '3' &&
          str[0] != '4' && str[0] != '5' && str[0] != '6' && str[0] != '7' &&
          str[0] != '8' && str[0] != '9' && !is_delimiter(str[0]));
}

bool is_keyword(char *element) {
  int i;
  char *keywords[] = {"entier",  "nombre",  "Si",       "Sinon",
                      "SinonSi", "TantQue", "Renvoyer", "Fun",
                      "Proc",    "Pour",    "de",       "a"};
  for (i = 0; i < sizeof(keywords) / sizeof(keywords[0]); ++i) {
    if (strcmp(element, keywords[i]) == 0) {
      return true;
    }
  }
  return false;
}

bool is_integer(char *str) {
  int i = 0;
  if (str == NULL || *str == '\0') {
    return false;
  }
  while (isdigit(str[i])) {
    ++i;
  }
  return str[i] == '\0';
}

char *get_substring(char *str, int start, int end) {
  int length = strlen(str);
  int sub_length = end - start + 1;
  char *sub_str = (char *)malloc((sub_length + 1) * sizeof(char));
  strncpy(sub_str, str + start, sub_length);
  sub_str[sub_length] = '\0';
  return sub_str;
}

int lexer(char *input) {
  int left = 0, right = 0;
  int len = strlen(input);

  while (right <= len && left <= right) {
    if (!is_delimiter(get_substring(input, left, right))) {
      right++;
    }
    if (is_delimiter(input[right]) && left == right) {
      if (isOperator(input[right]))
        printf("Token: Operator, Value: %c\n", input[right]);

      right++;
      left = right;
    } else if (isDelimiter(input[right]) && left != right ||
               (right == len && left != right)) {
      char *subStr = getSubstring(input, left, right - 1);

      if (isKeyword(subStr))
        printf("Token: Keyword, Value: %s\n", subStr);

      else if (isInteger(subStr))
        printf("Token: Integer, Value: %s\n", subStr);

      else if (isValidIdentifier(subStr) && !isDelimiter(input[right - 1]))
        printf("Token: Identifier, Value: %s\n", subStr);

      else if (!isValidIdentifier(subStr) && !isDelimiter(input[right - 1]))
        printf("Token: Unidentified, Value: %s\n", subStr);
      left = right;
    }
  }
  return 0;
}

int main() { return 0; }
