#include "include/lexer.h"
#include <stdlib.h>
#include <string.h>

lexer *init_lexer(char *src) {
    lexer *lexer = malloc(sizeof(struct LEXER_T));
    lexer->src = src;
    lexer->i = 0;
    lexer->c = src[0];
    return lexer;
}

void lexer_advance(lexer *lexer) {
    if (lexer->i < strlen(lexer->src) && lexer->c != '\0') {
        ++lexer->i;
        lexer->c = lexer->src[lexer->i];
    }
}

// TODO : gestion de l'identation
void lexer_eat_whitespace(lexer *lexer) {
    while (lexer->c == 13 || lexer->c == 10 || lexer->c == ' ' || lexer->c == '\t')
        lexer_advance(lexer);
}
