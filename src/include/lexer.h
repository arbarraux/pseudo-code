#ifndef LEXER_H_
#define LEXER_H_

typedef struct LEXER_T {
    char *src;
    unsigned int i;
    char c;
} lexer;

lexer *init_lexer(char *src);
void lexer_advance(lexer *lexer);
void lexer_eat_whitespace(lexer *lexer);

#endif // LEXER_H_
