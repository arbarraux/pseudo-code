#ifndef IO_H_
#define IO_H_

char *read_file(const char *filename);
void write_file(const char *filename, char *outbuffer);

#endif // IO_H_
